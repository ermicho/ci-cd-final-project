const fs = require('fs');



const readMessage = () => {
    try {
        const content = fs.readFileSync('../message.txt', 'utf-8');
        return content; 
    } catch (error) {
       return ''; 
    }
}


module.exports = { appendToFile, readMessage };
