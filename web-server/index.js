const http = require('http');
const fs = require('fs');

const readMessage = () => {
    try {
        const content = fs.readFileSync('../message.txt', 'utf-8');
        return content; 
    } catch (error) {
       return ''; 
    }
}

// create http server
http.createServer((req, res) => {
    const content = readMessage();
    if(req.method === 'GET' && req.url === '/'){
        res.write(content);
    }else{
        res.write('Http Server');
    }
    res.end();
}).listen(8080);