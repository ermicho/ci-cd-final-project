//create connection
const open = require('amqplib').connect('amqp://rabbitmq:5672');
const fs = require('fs');
// Publisher
const topic_1 = 'my.o';
const topic_2 = 'my.i';

const appendToFile = ( content) => {
  fs.appendFile('../message.txt', content, (err) => {
      if (err) {
          console.log(err);
        }
  })
}


//ORIG Section
open.then(function(conn) {
  // create and return a channel
  return conn.createChannel();
}).then(function(ch) {
  // check if channel with name exits
  return ch.assertQueue(topic_1).then(function(ok) {
    // send message to channel
      const handleInterval = () => {
        let i = 1;
        let interval;
        if(i < 4){
          interval = setInterval(() => {
                if(i >= 3) clearInterval(interval);
                ch.sendToQueue(topic_1, Buffer.from(`MSG_${i}`));
                i++;
              }, 3000);
          }
      }

      handleInterval();
  });
}).catch(console.warn);


// IMED Section
open.then(function(conn) {
  // create and return a channel
  return conn.createChannel();
}).then(function(ch) {
  // check if channel with name exists
  return ch.assertQueue(topic_1).then(function(ok) {
    // consume the message from topic one
    return ch.consume(topic_1, function(msg) {
      if (msg !== null) {
        return ch.assertQueue(topic_2).then(function(ok){
          const timer = setTimeout(() => {
            // send message to topic 2
            ch.sendToQueue(topic_2, Buffer.from(`Got ${msg.content.toString()}`));
            ch.ack(msg);
            clearTimeout(timer);
          }, 1000);
        })
      }
    });
  });
}).catch(console.warn);

// OBSE Section for my.o
open.then(function(conn) {
  return conn.createChannel();
}).then(function(ch) {
  return ch.assertQueue(topic_2).then(function(ok) {
    return ch.consume(topic_2, function(msg) {
      if (msg !== null) {
          const content = `${ new Date().toISOString()} Topic ${topic_2}: ${msg.content.toString()}\n`;
          // write output to file
          appendToFile(content);
      }
    });
  });
}).catch(console.warn);

// OBSE Section for my.i
open.then(function(conn) {
  return conn.createChannel();
}).then(function(ch) {
  return ch.assertQueue(topic_1).then(function(ok) {
    return ch.consume(topic_1, function(msg) {
      if (msg !== null) {
          const content = `${ new Date().toISOString()} Topic ${topic_1}: ${msg.content.toString()}\n`;
          // write output to file
          appendToFile(content);
      }
    });
  });
}).catch(console.warn);






module.exports = open;